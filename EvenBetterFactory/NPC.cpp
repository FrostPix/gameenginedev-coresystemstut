#include "NPC.hpp"

#include <iostream>

#include "GameObjectFactory.hpp"

NPC::NPC()
{

}

NPC::~NPC()
{

}

namespace
{

    GameObject* createNPC()
    {
        return new NPC();
    }

    /* This code 'somehow' ensures that registration will be involved before we enter the main. */
    bool s_bRegistered = GameObjectFactory::getInstance().Register("NPC",createNPC);
}

void NPC::draw()
{
    std::cout << "Drawing an NPC" << std::endl;
}

void NPC::update()
{
    std::cout << "Updating and NPC" << std::endl;
}
