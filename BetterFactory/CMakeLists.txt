cmake_minimum_required(VERSION 2.8.9)
project (BetterFactory)


# Compile the few cpp's we have into an exe. 
# No libraries here :-D
add_executable(BetterFactory GameObject.cpp NPC.cpp Rocket.cpp GameObjectFactory main.cpp)

# Make sure CMake can find the .h/.hpp files by explicitly asking it to 
# add the build directory (this could have been header/ or similar. 
target_include_directories (BetterFactory PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

